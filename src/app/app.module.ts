import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/material/material.module';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule} from "@angular/common/http";
import { FormsModule } from '@angular/forms';

import { MainComponent } from './components/main/main.component';

import { ClientesComponent } from './components/clientes/clientes.component';
import { VerInformacionComponent } from './components/dialogs/ver-informacion/ver-informacion.component';
import { NuevoClienteComponent } from './components/dialogs/nuevo-cliente/nuevo-cliente.component';
import { EditarClienteComponent } from './components/dialogs/editar-cliente/editar-cliente.component';
import { EditarTivComponent } from './components/dialogs/editar-tiv/editar-tiv.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ClientesComponent,
    VerInformacionComponent,
    NuevoClienteComponent,
    EditarClienteComponent,
    EditarTivComponent
  ],
  imports: [
    FormsModule,
    MatTableModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ToastrModule.forRoot(
      {
        timeOut: 3000,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false
      }),
  ],
  entryComponents: [EditarClienteComponent, NuevoClienteComponent, VerInformacionComponent, EditarTivComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
