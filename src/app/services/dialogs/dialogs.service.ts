import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { EditarClienteComponent } from 'src/app/components/dialogs/editar-cliente/editar-cliente.component';
import { EditarTivComponent } from 'src/app/components/dialogs/editar-tiv/editar-tiv.component';
import { NuevoClienteComponent } from 'src/app/components/dialogs/nuevo-cliente/nuevo-cliente.component';
import { VerInformacionComponent } from 'src/app/components/dialogs/ver-informacion/ver-informacion.component';

@Injectable({
  providedIn: 'root'
})
export class DialogsService {

  url='http://35.192.41.85/api/';
  //url = 'http://localhost:7000/'

  constructor(
    private dialog: MatDialog,
    private http: HttpClient
  ) {}

  openNuevoCliente() {
    return this.dialog.open(NuevoClienteComponent).afterClosed();
  }

  openEditarCliente(cliente) {
    let data = { cliente }
    return this.dialog.open(EditarClienteComponent, { data, disableClose: true }).afterClosed();
  }

  openEditarTiv(cliente) {
    let data = { cliente }
    return this.dialog.open(EditarTivComponent, { data, disableClose: true }).afterClosed();
  }

  openVerInfo(cliente) {
    let data = { cliente }
    return this.dialog.open(VerInformacionComponent, { data, disableClose: true }).afterClosed();
  }

  getAllCliente(){
    return this.http.get(`${this.url}cliente/`);  
  }

  getRepresentantes(){
    return this.http.get(`${this.url}cliente/representantes`);  
  }
  
  setCliente(params){
    return this.http.post(`${this.url}cliente/agregar`, params);  
  }
    
  updateCliente(params){
    return this.http.put(`${this.url}cliente/actualizar`, params);  
  }

  updateTiv(params){
    return this.http.put(`${this.url}cliente/tiv`, params);  
  }
}
