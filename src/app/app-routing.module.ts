import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { ClientesComponent } from './components/clientes/clientes.component';


const routes: Routes = [
  {path: "", redirectTo: "main", pathMatch: "full"},
  

  {path: "main", component: MainComponent, children: [
    {path: "clientes", component: ClientesComponent}
  ]},

  {path: "**", redirectTo: "main"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
