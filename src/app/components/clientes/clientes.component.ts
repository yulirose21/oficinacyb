import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from 'src/app/services/dialogs/dialogs.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})

export class ClientesComponent implements OnInit {

  displayedColumns = ['id', 'name','atv', 'tiv','acciones'];
  dataSource;
  ELEMENT_DATA: any[];

  constructor(
    private dialog: DialogsService,
    private toast: ToastrService,
  ) {
    setInterval(() => this.ngOnInit(), 5000);
  }

  ngOnInit() {
    this.getAllCliente();
  }

  openVerInfo(cliente: number){
    this.dialog.openVerInfo(cliente);
  }

  openEditarTiv(cliente: number){
    this.dialog.openEditarTiv(cliente)
    .subscribe((response: boolean) => {
      if(response){
        this.toast.success("La acción fue confirmada")
        //this.getAllCliente();
      }
      else
        this.toast.info("La acción fue cancelada")   
    })
  }

  openEditarCliente(cliente: number){
    this.dialog.openEditarCliente(cliente)
    .subscribe((response: boolean) => {
      if(response){
        this.toast.success("La acción fue confirmada")
        //this.getAllCliente();
      }
      else
        this.toast.info("La acción fue cancelada")   
    })
  }
  
  openNuevoCliente(){
    this.dialog.openNuevoCliente()
    .subscribe((response: boolean) => {
      if(response){
        this.toast.success("La acción fue confirmada")
        //this.getAllCliente();
      }
      else
        this.toast.info("La acción fue cancelada")   
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllCliente(){
    this.ELEMENT_DATA = []; 
    this.dialog.getAllCliente()
    .subscribe((response: any) => {
      if(response.clientes){
        response.clientes.forEach(element => {
          let nacimiento = element.fecha_nacimiento,
          cedula = element.fecha_vencimiento;

          if(element.fecha_nacimiento!=null){
            nacimiento = element.fecha_nacimiento.split(['T'])[0]
          }

          if(element.fecha_vencimiento!=null){
            cedula = element.fecha_vencimiento.split(['T'])[0]
          }

          var cliente = {
            'id_cliente':element.id_cliente,
            'nombre':element.nombre, 
            'atv':element.atv, 
            'tiv': [element.a , element.b, element.c , element.d, 
              element.e , element.f, element.g , element.h, element.i, 
              element.j, element.k , element.l, element.m , element.n, element.o],  
            'estado': element.estado,
            'id_tiv': element.id_tiv,
            'tipo': element.tipo,
            'representante_legal': element.representante_legal,
            'fecha_nacimiento': nacimiento,
            'fecha_vencimiento': cedula,
            'correo_contra': element.correo_contra,
            'email': element.email,
            'numero': element.numero,
            'usuario': element.usuario,
            'nombre_fact': element.nombre_fact,
            'facturador_contra': element.facturador_contra
          };
          this.ELEMENT_DATA.push(cliente);
        });}
      else
        this.toast.info("Error al cargar los clientes")    
      this.ELEMENT_DATA.sort((a,b) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    })
  }
}