import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from 'src/app/services/dialogs/dialogs.service';

@Component({
  selector: 'app-editar-tiv',
  templateUrl: './editar-tiv.component.html',
  styleUrls: ['./editar-tiv.component.css']
})
export class EditarTivComponent implements OnInit {
  t1;t2;t3;t4;t5;t6;t7;t8;t9;t10;t11;t12;t13;t14;t15;

  constructor(
    public dialogRef: MatDialogRef<EditarTivComponent>,
    private dialog: DialogsService,
    private toast: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.t1 = this.data.cliente.tiv[0];
    this.t2 = this.data.cliente.tiv[1];
    this.t3 = this.data.cliente.tiv[2];
    this.t4 = this.data.cliente.tiv[3];
    this.t5 = this.data.cliente.tiv[4];
    this.t6 = this.data.cliente.tiv[5];
    this.t7 = this.data.cliente.tiv[6];
    this.t8 = this.data.cliente.tiv[7];
    this.t9 = this.data.cliente.tiv[8];
    this.t10 = this.data.cliente.tiv[9];
    this.t11 = this.data.cliente.tiv[10];
    this.t12 = this.data.cliente.tiv[11];
    this.t13 = this.data.cliente.tiv[12];
    this.t14 = this.data.cliente.tiv[13];
    this.t15 = this.data.cliente.tiv[14];
  }

  yes(){
    let params = {
      'id_cliente': this.data.cliente.id_cliente,
      'a': this.t1,
      'b': this.t2, 
      'c': this.t3,
      'd': this.t4, 
      'e': this.t5, 
      'f': this.t6, 
      'g': this.t7,
      'h': this.t8, 
      'i': this.t9,
      'j': this.t10,
      'k': this.t11,        
      'l': this.t12,
      'm': this.t13,
      'n': this.t14,
      'o': this.t15
    }
    console.log(params)
    try {
      this.dialog.updateTiv(params)
      .subscribe((response) => {
        if(response == 204){
          this.dialogRef.close(true);        
        }
        else if(response == 400){
          this.toast.error("Error");
        }
      })     
    } catch(e) {
      this.toast.error("Datos erroneos"); 
    }
    this.dialogRef.close(true)
  }


  no(){
    this.dialogRef.close(false)
  }

}
