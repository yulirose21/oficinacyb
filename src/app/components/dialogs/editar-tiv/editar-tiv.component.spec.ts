import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarTivComponent } from './editar-tiv.component';

describe('EditarTivComponent', () => {
  let component: EditarTivComponent;
  let fixture: ComponentFixture<EditarTivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarTivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarTivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
