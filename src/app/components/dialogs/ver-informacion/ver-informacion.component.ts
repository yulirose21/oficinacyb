import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from 'src/app/services/dialogs/dialogs.service';

@Component({
  selector: 'app-ver-informacion',
  templateUrl: './ver-informacion.component.html',
  styleUrls: ['./ver-informacion.component.css']
})
export class VerInformacionComponent implements OnInit {
  tipo; estado; nombre;
  representantes = [];
  constructor(
    public dialogRef: MatDialogRef<VerInformacionComponent>,
    private dialog: DialogsService,
    private toast: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.getRepresentante();
    if (this.data.cliente.estado == 0){
      this.estado = 'Tradicional'
    }
    if (this.data.cliente.estado == 1){
      this.estado = 'Simplificado'
    }
    if (this.data.cliente.estado == 2){
      this.estado = 'REA'
    }
    if (this.data.cliente.tipo == 0){
      this.tipo = 'Persona juridica'
    }
    if (this.data.cliente.tipo == 1) {
      this.tipo = 'Persona Fisica'
    }
  }
  
  getRepresentante(){
    this.dialog.getRepresentantes()
    .subscribe((response: any) => {
      if(response.clientes){
        response.clientes.forEach(element => {
          var cliente = {
            'id_cliente':element.id_cliente,
            'nombre':element.nombre, 
          };
          this.representantes.push(cliente);
        });
        this.representantes.forEach(element => {
          if (element.id_cliente == this.data.cliente.representante_legal){
            this.nombre = element.nombre
          }
        });
      }
      else
        this.toast.info("Error al cargar los clientes")    
    })
  }
  
  yes(){
    this.dialogRef.close(true)
  }

  no(){
    this.dialogRef.close(false)
  }

}