import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from 'src/app/services/dialogs/dialogs.service';


@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {
  atv; correo_contra; email; estado; facturador_contra; fecha_nacimiento; fecha_vencimiento; id_cliente;
  nombre; nombre_fact; numero; representante_legal; usuario;
  status; opcion; representantes = [];
  
  constructor(
    public dialogRef: MatDialogRef<EditarClienteComponent>,
    private dialog: DialogsService,
    private toast: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.getRepresentantes();
    this.atv = this.data.cliente.atv; 
    this.correo_contra = this.data.cliente.correo_contra; 
    this.email = this.data.cliente.email; 
    this.estado = this.data.cliente.estado; 
    this.facturador_contra = this.data.cliente.facturador_contra; 
    this.fecha_nacimiento = this.data.cliente.fecha_nacimiento; 
    this.fecha_vencimiento = this.data.cliente.fecha_vencimiento; 
    this.id_cliente = this.data.cliente.id_cliente;
    this.nombre = this.data.cliente.nombre; 
    this.nombre_fact = this.data.cliente.nombre_fact; 
    this.numero = this.data.cliente.numero; 
    this.representante_legal = this.data.cliente.representante_legal; 
    this.usuario = this.data.cliente.usuario;
    this.status = this.data.cliente.status; 
    this.opcion = this.data.cliente.opcion;
  }

  getRepresentantes(){
    this.dialog.getRepresentantes()
    .subscribe((response: any) => {
      if(response.clientes){
        response.clientes.forEach(element => {
          var cliente = {
            'id_cliente':element.id_cliente,
            'nombre':element.nombre, 
          };
          this.representantes.push(cliente);
        });
        this.representantes.sort((a,b) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      }
      else
        this.toast.info("Error al cargar los clientes")    
    })
  }

  yes(){
    let params = {
      'id_cliente': parseInt(this.id_cliente),
      'nombre': this.nombre, 
      'atv': this.atv,
      'fecha_nacimiento': this.fecha_nacimiento, 
      'fecha_vencimiento': this.fecha_vencimiento, 
      'representante_legal': parseInt(this.representante_legal), 
      'estado': parseInt(this.estado), 
      'email': this.email,
      'correo_contra': this.correo_contra,
      'numero': this.numero,        
      'nombre_fact': this.nombre_fact,
      'usuario': this.usuario,
      'facturador_contra': this.facturador_contra
    }

    try {
      this.dialog.updateCliente(params)
      .subscribe((response) => {
        if(response == 204){
          this.dialogRef.close(true);        
        }
        else if(response == 400){
          this.toast.error("Error");
        }
      })     
    } catch(e) {
      this.toast.error("Datos erroneos"); 
    }
      this.dialogRef.close(true)
  }

  no(){
    this.dialogRef.close(false)
  }

}
