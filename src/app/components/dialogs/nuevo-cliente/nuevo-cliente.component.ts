import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { DialogsService } from 'src/app/services/dialogs/dialogs.service';

@Component({
  selector: 'app-nuevo-cliente',
  templateUrl: './nuevo-cliente.component.html',
  styleUrls: ['./nuevo-cliente.component.css']
})

export class NuevoClienteComponent implements OnInit {
    atv; correo_contra; email; estado; facturador_contra; fecha_nacimiento; fecha_vencimiento; id_cliente;
    nombre; nombre_fact; numero; representante_legal; tipo; usuario;
    status; representante; representantes = [];

  constructor(
    public dialogRef: MatDialogRef<NuevoClienteComponent>,
    private dialog: DialogsService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.getRepresentantes();
  }

  yes(){
    let params = {
      id_cliente: parseInt(this.id_cliente),
      nombre: this.nombre, 
      atv: this.atv,
      fecha_nacimiento: this.fecha_nacimiento, 
      fecha_vencimiento: this.fecha_vencimiento, 
      representante_legal: 0, 
      estado: parseInt(this.estado),
      tipo: parseInt(this.tipo), 
      email: this.email,
      correo_contra: this.correo_contra,
      numero: this.numero,        
      nombre_fact: this.nombre_fact,
      usuario: this.usuario,
      facturador_contra: this.facturador_contra
    }
    try {
      this.dialog.setCliente(params)
      .subscribe((response) => {
        if(response == 201){
          this.dialogRef.close(true);        
        }
        else if(response == 400){
          this.toast.error("Error");
        }
      })     
    } catch(e) {
      this.toast.error("Datos erroneos"); 
    }
    this.dialogRef.close(true)
  }

  no(){
    this.dialogRef.close(false)
  }

  getRepresentantes(){
    this.dialog.getRepresentantes()
    .subscribe((response: any) => {
      if(response.clientes){
        response.clientes.forEach(element => {
          var cliente = {
            'id_cliente':element.id_cliente,
            'nombre':element.nombre, 
          };
          this.representantes.push(cliente);
        });
        this.representantes.sort((a,b) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      }
      else
        this.toast.info("Error al cargar los clientes")    
    })
  }
}
