import { Component, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  mobileQuery: MediaQueryList;

  items = [
    {name: "Clientes", icon: "people", route:"/main/clientes"},
  ]

  constructor( media: MediaMatcher) {
    
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
   }

  ngOnInit() {
    
  }

}
