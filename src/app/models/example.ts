export interface Example{
    a1: number
    a2: string
    a3: {a31: string, a32: number}
    a4: boolean
}